#!/bin/python

import sys

if __name__ == "__main__":
    n, m = raw_input().strip().split(' ')
    n, m = [int(n), int(m)]
    li=[]
    
    for i in range(0,n):
        li.append(0)

    for a0 in xrange(m):
        a, b, k = raw_input().strip().split(' ')
        a, b, k = [int(a), int(b), int(k)]
        for i in range(a-1,b):
            li[i]+=k
    print max(li)
    


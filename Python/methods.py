class Test(object):
	def method_one(self,var1):
		self.var1=var1
		print "Called method_one"
		print var1

  	def method_two(self,var2):
  		self.var2=var2
		print "Called method_two"
		print self

	def method_three(arg):
		#cls.var3=var3
		print "Called Class Method - method_three"
		#print var3
		print cls


a_test = Test()
print
print Test.method_three
print 
a_test.method_one(1)
a_test.method_two(2)
#Test.method_one(a_test,1)
Test.method_three()	


#print dir()''
from flask import Flask, render_template,request,json,redirect,url_for
from pymongo import MongoClient
import os

def connect():
	connection=MongoClient("0.0.0.0",27017)
	return connection

app = Flask(__name__)


@app.route("/")
def welc():
	return render_template("index.html")

@app.route("/showsignup.html")
def showsignup():
	return render_template("signup.html")

@app.route("/signup",methods=['POST'])
def signup():
	client=connect()
	db=client["phalgun_test"]["exp"]
	_name = request.form['name']
	_email = request.form['email']
	_password = request.form['psw']
	print "WAIT1"

	if _name and _email and _password:
		db.insert([{"name":_name,
				"email":_email,
				"password":_password}])

		# db.insert({"email":_email})
		# db.insert({"password":_password})
		return json.dumps({'html':'<span>All fields good !!</span>'})	
	else:
		return json.dumps({'html':'<span>Enter the required fields</span>'})

@app.route("/showlogin.html")				
def showlogin():
	return render_template('login.html')

@app.route("/login",methods=['POST'])
def login():
	client=connect()
	db=client["phalgun_test"]["exp"]
	_email = request.form['username']
	_password = request.form['password']
	userobj=db.find_one({"email":_email})
	if userobj:
		print "--------------------------------Name Equal"
		if userobj['password']==_password:
			#return json.dumps("Authentic User")
			return "success"
		return json.dumps("Username Exists password mismatch")
	else:
		print "Name Not Found"
		return json.dumps("Wrong Username")

@app.route("/showprofile.html")
def showprofile():
	return render_template("profile.html")


if __name__=="__main__":
	app.run()
from fpdf import FPDF
import sys
import datetime
import inwords

#this is strictly A4 sheet
#form has 2 vertical compartments each compartment width 90
#Limit No of Characters in Table Description to 90 to avoid undesirable effects
#Max Rows(Items + Tax Rows) Tested Upto 25 Rows

def pdfrun():
	#words=Number2Words()	
	compwidth=90
	Lmargin=8
	gap=7
	x=Lmargin
	hw1=24
	hw2=105
	hw3=30
	hw4=30
	hh=10
	ch=20
	sum1=0

	Lmargin2=Lmargin+compwidth+gap
	pdf = FPDF('P', 'mm', 'A4')
	#pdf.set_top_margin(3)
	pdf.add_page()
	pdf.set_display_mode("fullpage", "default")
	pdf.set_font("Times", "", 12)
	print("X-Coordinate:")
	print(pdf.get_x())
	print("Y-Coordinate:")
	print(pdf.get_y())
	pdf.image("/Users/phalgun/Downloads/nprodax1.jpg",x,3,65,25)
	pdf.set_xy(Lmargin2,4)
	pdf.image("/Users/phalgun/Downloads/gps.png",Lmargin2,4,4,4)
	pdf.set_xy(Lmargin2+4,4)
	pdf.multi_cell(85,5, ""+caddr, 0,'J',  False)
	pdf.image("/Users/phalgun/Downloads/phone.jpg",Lmargin2,18,4,4)
	pdf.set_xy(Lmargin2+4,15)
	pdf.cell(40, 10, ""+cphone,0,1)       
	pdf.image("/Users/phalgun/Downloads/globe.png",155,18,4,4)
	pdf.set_xy(159,15)
	pdf.cell(40, 10, ""+cweb,0,1)
	pdf.set_font("Times", "B", 34)
	pdf.set_xy(125,20)
	pdf.cell(40, 30, "Invoice",0,1)
	pdf.set_xy(103,55)
	pdf.set_font("Times", "", 12)
	pdf.cell(40, 5, "Date: "+date ,0,1)
	pdf.set_xy(103,60)
	pdf.multi_cell(90, 5, "Invoice Number: "+inb,0)
	pdf.set_xy(103,65)
	pdf.multi_cell(90, 5, "PO Number: "+pnb,0)
	pdf.set_xy(Lmargin,80)
	pdf.cell(40, 5, "To:",0,1)
	pdf.set_xy(Lmargin,85)
	pdf.multi_cell(90, 5, ""+toaddr,0)
	pdf.set_xy(103,80)
	pdf.cell(40, 5, "For:",0,1)
	pdf.set_xy(103,85)
	pdf.multi_cell(90, 5, ""+forserve,0)
	x=Lmargin
	y=110
	pdf.set_xy(x,y)
	
	#HEADER ROWS
	pdf.cell(hw1, hh, "Quantity",1,1,"C")
	x+=hw1
	pdf.set_xy(x,y)
	pdf.cell(hw2, hh, "Description",1,1,"C")
	x+=hw2
	pdf.set_xy(x,y)
	pdf.cell(hw3, hh, "Amount",1,1,"C")
	x+=hw3
	pdf.set_xy(x,y)
	pdf.cell(hw4, hh, "Total",1,1,"C")

	#TABLE CONTENTS
	nb=(int(input("Enter no of items (>0) \n")))
	y+=hh
	pdf.set_font("Times", "", 14)
	for i in range(1,nb+1):
		x=Lmargin
		pdf.set_xy(x,y)
		pdf.cell(hw1, ch, str(i),1,1)
		x+=hw1
		pdf.set_xy(x,y)
		pdf.cell(hw2, ch,"",1,1)
		pdf.set_xy(x,y)
		pdf.multi_cell(hw2, 10, str(raw_input("\nEnter Description for Item: "+str(i)+"\n")) ,0,"C")
		x+=hw2
		pdf.set_xy(x,y)
		pdf.cell(hw3, ch, str(raw_input("\nEnter Amount for Item: "+str(i))+"\n"),1,1)
		x+=hw3
		pdf.set_xy(x,y)
		tsum=int(raw_input("\nEnter Total for Item: "+str(i)+"\n"))
		sum1+=tsum
		pdf.cell(hw4, ch, str(tsum),1,1)
		y+=ch
		if i==7 or i==20:		
			pdf.add_page()
			x=Lmargin
			y=10
		
	x=Lmargin
	pdf.set_xy(x,y)
	#pdf.cell(hw4, ch, "Hello ",1,1)
	if y>=260:
		pdf.add_page()
		x=Lmargin
		y=10

	pdf.set_xy(x,y)
	#TAX ROW1
	pdf.multi_cell(hw1+hw2+hw3,ch, "SUBTOTAL",1,"R")
	x+=hw1+hw2+hw3
	pdf.set_xy(x,y)
	pdf.multi_cell(hw4,ch, ""+str(sum1),1,"C")
	y+=ch
	if y>=260:
		pdf.add_page()
		x=Lmargin
		y=10

	#TAX ROW2
	x=Lmargin
	pdf.set_xy(x,y)
	pdf.multi_cell(hw1+hw2+hw3, ch, "TAX(14%)",1,"R")
	x+=hw1+hw2+hw3
	pdf.set_xy(x,y)
	tax=float((14.0/100)*sum1)
	pdf.multi_cell(hw4, ch, ""+str(tax),1,"C")
	y+=ch
	if y>=260:
		pdf.add_page()
		x=Lmargin
		y=10


	#TAX ROW 3
	x=Lmargin
	pdf.set_xy(x,y)
	pdf.multi_cell(hw1+hw2+hw3, ch, "SWATCHH BHARAT CESS(0.5%)",1,"R")
	x+=hw1+hw2+hw3
	pdf.set_xy(x,y)
	swtax=float((0.5/100)*sum1)
	pdf.multi_cell(hw4, ch, ""+str(swtax),1,"C")
	y+=ch
	if y>=260:
		pdf.add_page()
		x=Lmargin
		y=10


	#TAX ROW 4
	x=Lmargin
	pdf.set_xy(x,y)
	pdf.multi_cell(hw1+hw2+hw3, ch, "KRISHI KALYAN CESS(0.5%)",1,"R")
	x+=hw1+hw2+hw3
	pdf.set_xy(x,y)
	ktax=float((0.5/100)*sum1)
	pdf.multi_cell(hw4, ch, ""+str(ktax),1,"C")
	y+=ch
	if y>=260:
		pdf.add_page()
		x=Lmargin
		y=10


	#TAX ROW 5
	x=Lmargin
	pdf.set_xy(x,y)
	pdf.multi_cell(hw1+hw2+hw3, ch, "TOTAL",1,"R")
	x+=hw1+hw2+hw3
	pdf.set_xy(x,y)
	pdf.multi_cell(hw4, ch, ""+str(float(tax+swtax+ktax+sum1)),1,"C")
	y+=ch
	if y>=260:
		pdf.add_page()
		x=Lmargin
		y=10


	#IN WORDS
	x=Lmargin
	pdf.set_xy(x,y)
	pdf.multi_cell(0, ch, "In Words: "+str(words.convertNumberToWords(float(tax+swtax+ktax+sum1))),0,1)


	# #ROW 1
	# pdf.set_font("Times", "B", 14)
	# pdf.multi_cell(40, 10, "Quantity",1,"C")
	# pdf.set_xy(63,110)
	# pdf.multi_cell(40, 10, "Description",1,"C")
	# pdf.set_xy(103,110)
	# pdf.multi_cell(40, 10, "Amount",1,"C")
	# pdf.set_xy(143,110)
	# pdf.multi_cell(40, 10, "Total in INR",1,"C")
	
	# #ROW 2
	# pdf.set_font("Times", "", 14)
	# pdf.set_xy(23,120)
	# pdf.multi_cell(40, 20, "1",1,"C")
	# pdf.set_xy(63,120)
	# pdf.multi_cell(40, 5, ""+supplier_name,0,"C")
	# pdf.set_xy(103,120)
	# pdf.multi_cell(40, 20, "100000",1,"C")
	# pdf.set_xy(143,120)
	# pdf.multi_cell(40, 20, "100000",1,"C")
	# pdf.set_xy(23,140)
	# pdf.multi_cell(120, 10, "SUBTOTAL",1,"R")
	# pdf.set_xy(143,140)
	# pdf.multi_cell(40, 10, "100000",1,"C")
	# pdf.set_xy(23,150)
	# pdf.multi_cell(120, 10, "TAX(14%)",1,"R")
	# pdf.set_xy(143,150)
	# pdf.multi_cell(40, 10, "14000",1,"C")
	# pdf.set_xy(23,160)
	# pdf.multi_cell(120, 10, "SWATCHH BHARAT CESS(0.5%)",1,"R")
	# pdf.set_xy(143,160)
	# pdf.multi_cell(40, 10, "500",1,"C")
	# pdf.set_xy(23,170)
	# pdf.multi_cell(120, 10, "KRISHI KALYAN CESS(0.5%)",1,"R")
	# pdf.set_xy(143,170)
	# pdf.multi_cell(40, 10, "500",1,"C")
	# pdf.set_xy(23,180)
	# pdf.multi_cell(120, 10, "TOTAL",1,"R")
	# pdf.set_xy(143,180)
	# pdf.multi_cell(40, 10, "115000",1,"C")
	# pdf.set_xy(23,190)
	# pdf.multi_cell(0, 10, "In Words: One Lakhs Fifteen Thousand Only",0,1)
	#sys.exit("error: bad boy")





	pdf.output("/Users/phalgun/Desktop/Programs/dailywork/fpdf-work/one.pdf","F")

now = datetime.datetime.now()
date = str(now)
caddr=str(raw_input("\nEnter Company Address\n"))
cphone=str(raw_input("\nEnter Company Phone\n"))
cweb=str(raw_input("\nEnter Company Web Address\n"))
toaddr=str(raw_input("\nEnter 'To' Address\n"))
forserve=str(raw_input("\nEnter 'For Service' Description\n"))
inb=str(raw_input("\nEnter Invoice Number\n"))
pnb=str(raw_input("\nEnter PO Number\n"))
pdfrun()

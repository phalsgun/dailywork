from fpdf import FPDF


#Here "Lmargin" AND "y"  DECIDES THE ORIGIN COORDINATES OF TABLE 

#TABLE PROPERTIES
Lmargin=10
hw1=24
hw2=105
hw3=30
hw4=30
hh=10
ch=20

#TABLE POSITION IN PAGE
x=Lmargin
y=60

pdf = FPDF('P', 'mm', 'A4')
pdf.add_page()
pdf.set_font("Times", "B", 15)	
pdf.set_xy(x ,y)

#HEADER ROWS
pdf.cell(hw1, hh, "Quantity",1,1,"C")
x+=hw1
pdf.set_xy(x,y)
pdf.cell(hw2, hh, "Description",1,1,"C")
x+=hw2
pdf.set_xy(x,y)
pdf.cell(hw3, hh, "Amount",1,1,"C")
x+=hw3
pdf.set_xy(x,y)
pdf.cell(hw4, hh, "Total",1,1,"C")

#TABLE CONTENTS
nb=(int(input("Enter no of items (>0) \n")))
y+=hh
pdf.set_font("Times", "", 14)	
for i in range(1,nb+1):
	x=Lmargin
	pdf.set_xy(x,y)

	pdf.cell(hw1, ch, ""+str(x)+" "+str(y),1,1)
	x+=hw1
	pdf.set_xy(x,y)
	pdf.cell(hw2, ch, ""+str(x)+" "+str(y),1,1)
	x+=hw2
	pdf.set_xy(x,y)
	pdf.cell(hw3, ch, ""+str(x)+" "+str(y),1,1)
	x+=hw3
	pdf.set_xy(x,y)
	pdf.cell(hw4, ch, ""+str(x)+" "+str(y),1,1)
	y+=ch
	if i==8:		
		pdf.add_page()
		x=Lmargin
		y=60
		
pdf.output("/Users/phalgun/Desktop/Programs/dailywork/fpdf-work/table.pdf","F")
